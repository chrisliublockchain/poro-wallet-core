module.exports = {
  preset: 'ts-jest',
  testTimeout: 1000000,
  testEnvironment: 'node',
  setupFiles: ["dotenv/config"],
  testMatch: ["**/?(*.test.ts)"],
  moduleFileExtensions: ["js", "ts", "json", "node"],
};
