# @poro-wallet/core

[![npm](https://badge.fury.io/js/@poro-wallet%2Fcore.svg)](https://badge.fury.io/js/@poro-wallet%2Fcore)

Core package aimed at facilitating blockchain wallets and transactions management, such as generating random wallet and constructing transaction objects used in offline signing for various supported blockchains.

## Getting started

```
npm install --save @poro-wallet/core
```

[Documentation](https://nighostchris.gitbook.io/poro-wallet-core/)
