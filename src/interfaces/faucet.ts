export interface FaucetResponse {
  success: boolean
  timestamp?: number
  message?: string
}
