export type ErcStandard = 'erc-20' | 'erc-721' | 'erc-1155';

export interface EvmNativeTransactionParams {
  to: string
  from: string
  amount: string
}

export interface TransferFunctionParams {
  to: string
  id?: string
  from?: string
  data?: string
  value?: string
  tokenId?: string
}

// https://eips.ethereum.org/EIPS/eip-20
export interface Erc20TransferFunctionParams {
  to: string
  value: string
}

// https://eips.ethereum.org/EIPS/eip-721
export interface Erc721SafeTransferFromFunctionParams {
  to: string
  from: string
  tokenId: string
}

// https://eips.ethereum.org/EIPS/eip-1155
export interface Erc1155SafeTransferFromFunctionParams {
  to: string
  id: string
  from: string
  value: string
  data?: string
}

export interface EvmTokenTransactionParams<T extends TransferFunctionParams> {
  from: string
  functionParams: T
  contractAddress: string
}

export interface EvmBuiltTx {
  to: string
  type: number
  nonce: string
  value: string
  chainId: number
  gasLimit: string
  maxFeePerGas: string
  maxPriorityFeePerGas: string
  data?: string
}
