export type CosmosMessageType = '/cosmos.bank.v1beta1.MsgSend';

export interface CosmosBankMessageSendParams {
  to: string
  from: string
  amount: string
}

export interface CosmosAmount {
  denom: string
  amount: string
}

export interface CosmosMsgSend {
  amount: CosmosAmount[]
  toAddress: string
  fromAddress: string
}

export interface CosmosBuiltTx {
  value: CosmosMsgSend
  typeUrl: CosmosMessageType
}
