export interface RippleNativeTransactionParams {
  to: string
  from: string
  amount: string
}

export interface RippleTokenTransactionParams extends RippleNativeTransactionParams {
  currency: string
  contractAddress: string
}

export interface RippleTokenOfferCreateTransactionParams extends RippleNativeTransactionParams {
  currency: string
}

export interface RippleAmount {
  value: string
  issuer: string
  currency: string
}

export interface RipplePayment {
  Fee: string
  Flags: number
  Account: string
  Sequence: number
  Destination: string
  TransactionType: string // 'Payment'
  Amount: string | RippleAmount
}

export interface RippleOfferCreate {
  Fee: string
  Flags: number
  Account: string
  Sequence: number
  TransactionType: string // 'OfferCreate'
  TakerPays: RippleAmount
  TakerGets: string | RippleAmount
}

export type RippleBuiltTx = RipplePayment | RippleOfferCreate;
