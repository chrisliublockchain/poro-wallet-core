export interface Balance {
  ticker: string
  amount: string
  decimals: string
}
