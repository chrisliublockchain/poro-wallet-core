export type HexString = string & { readonly HexString: unique symbol };
export type EvmAddress = string & { readonly EvmAddress: unique symbol };
export type RippleAddress = string & { readonly RippleAddress: unique symbol };
export type CosmosAddress = string & { readonly CosmosAddress: unique symbol };
export type StellarAddress = string & { readonly StellarAddress: unique symbol };
