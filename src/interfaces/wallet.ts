import { CosmosAddress, EvmAddress, RippleAddress, StellarAddress } from './data-type';

export interface Wallet {
  address: string
  mnemonic: string
  publicKey: string
  privateKey: string
}

export interface EvmWallet extends Wallet {
  address: EvmAddress
}

export interface RippleWallet extends Wallet {
  address: RippleAddress
}

export interface StellarWallet extends Wallet {
  address: StellarAddress
}

export interface CosmosWallet extends Wallet {
  address: CosmosAddress
}
