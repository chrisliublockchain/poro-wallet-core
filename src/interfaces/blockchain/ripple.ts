export interface RippleAccountData {
  index: string
  Flags: number
  Domain: string
  Account: string
  Balance: string
  Sequence: number
  OwnerCount: number
  PreviousTxnID: string
  LedgerEntryType: string
  PreviousTxnLgrSeq: number
}

export interface RippleAccountInfoResponse {
  result: {
    queue_data?: {
      txn_count: number
    }
    status: string
    error?: string
    account?: string // Only exists in 'Account not found.' response
    validated?: boolean
    error_code?: number
    error_message?: string
    ledger_current_index?: number
    account_data?: RippleAccountData
  }
}

export interface RippleAccountLine {
  limit: string
  account: string
  balance: string
  currency: string
  freeze?: boolean
  quality_in: number
  limit_peer: string
  quality_out: number
  no_ripple?: boolean
  authorized?: boolean
  freeze_peer?: boolean
  no_ripple_peer?: boolean
  peer_authorized?: boolean
}

export interface RippleAccountLinesResponse {
  result: {
    status: string
    error?: string
    account: string
    error_code?: number
    validated?: boolean
    error_message?: string
    lines?: RippleAccountLine[]
    ledger_current_index?: number
  }
}
