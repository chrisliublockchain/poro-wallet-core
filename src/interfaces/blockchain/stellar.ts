export interface StellarAccountInfoResponse {
  id: string
  account_id: string
  sequence: number
  subentry_count: number
  last_modified_ledger: number
  num_sponsoring: number
  num_sponsored: number
  sponsor?: string
  threshold: {
    low_threshold: number
    med_threshold: number
    high_threshold: number
  }
  flags: {
    auth_required: boolean
    auth_revocable: boolean
    auth_immutable: boolean
    auth_clawback_enabled: boolean
  }
  balances: Array<{
    limit?: string
    balance: string
    sponsor?: string
    asset_type: 'native' | 'credit_alphanum4' | 'credit_alphanum12' | 'liquidity_pool_shares'
    asset_code?: string
    asset_issuer?: string
    is_authorized: boolean
    buying_liabilities?: string
    selling_liabilities?: string
    last_modified_ledger: number
    is_authorized_to_maintain_liabilities: boolean
  }>
}
