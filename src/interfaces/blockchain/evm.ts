import { ethers } from 'ethers';

export interface GetEvmBalanceParamsContract {
  ticker: string
  address: string
  abi: ethers.ContractInterface
}

export interface GetEvmBalanceParams {
  node: string
  account: string
  contracts: GetEvmBalanceParamsContract[]
}
