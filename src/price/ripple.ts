/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import BigNumber from 'bignumber.js';
import { ServerInfoResponse, BookOffersResponse } from 'xrpl/dist/npm/models/methods';

import {
  BASE_UNIT,
  RIPPLE_DEFAULT_LOAD_FACTOR,
  RIPPLE_DEFAULT_BASE_FEE as base_fee_xrp,
} from '../constants';

// Get transaction fee from server
// https://xrpl.org/server_info.html
export const fee = async (node: string): Promise<string> => {
  const { data }: { data: ServerInfoResponse } = await axios.get(node, {
    headers: {
      'Content-Type': 'application/json',
    },
    data: {
      method: 'server_info',
      params: [{}],
    },
  });

  const loadFactor = data.result.info.load_factor ?? RIPPLE_DEFAULT_LOAD_FACTOR;
  const baseFee = (data.result.info.validated_ledger ?? data.result.info.closed_ledger ?? { base_fee_xrp }).base_fee_xrp;
  return new BigNumber(loadFactor).multipliedBy(new BigNumber(baseFee)).toString();
}

// Using native token as reference for exchange rate
// https://xrpl.org/book_offers.html
export const token = async (
  node: string,
  currency: string,
  contractAddress: string,
): Promise<string> => {
  const { data }: { data: BookOffersResponse } = await axios.get(node, {
    headers: {
      'Content-Type': 'application/json',
    },
    data: {
      method: 'book_offers',
      params: [{
        taker: contractAddress,
        taker_gets: {
          currency: 'XRP',
        },
        taker_pays: {
          currency,
          issuer: contractAddress,
          value: 1,
        },
        limit: 20,
      }],
    },
  });

  const orderBookList: string[] = data.result.offers.map(({ quality }) => (
    new BigNumber(quality ?? '0').multipliedBy(BASE_UNIT.RIPPLE).toString()));

  if (orderBookList.length === 0) {
    throw Error(`Cannot find order book history for XRP:${currency} transactions`);
  }
  return orderBookList[orderBookList.length / 2 | 0];
}
