/* eslint-disable import/prefer-default-export */
import { providers } from 'ethers';

export const fee = async (node: string): Promise<{ maxFeePerGas: string, maxPriorityFeePerGas: string }> => {
  const provider = new providers.JsonRpcProvider(node);
  const { maxFeePerGas, maxPriorityFeePerGas } = await provider.getFeeData();

  if (maxFeePerGas === null || maxPriorityFeePerGas === null) {
    throw Error('Cannot get fee data');
  }

  return {
    maxFeePerGas: maxFeePerGas.toHexString(),
    maxPriorityFeePerGas: maxPriorityFeePerGas.toHexString(),
  };
}
