import BigNumber from 'bignumber.js';

// Blockchain related constants
// Cosmos
export const COSMOS_DEFAULT_GAS = 500;
export const COSMOS_DEFAULT_GAS_LIMIT = 200000;
// EVM Chains
export const ETHEREUM_DEFAULT_GAS_LIMIT = 200000;
export enum ABI_FUNCTION_INTERFACE {
  ERC20_TRANSFER = 'function transfer(address to, uint256 value)',
  ERC721_SAFE_TRANSFER_FROM = 'function safeTransferFrom(address from, address to, uint256 tokenId)',
  ERC1155_SAFE_TRANSFER_FROM = 'function safeTransferFrom(address from, address to, uint256 id, uint256 value, bytes data)',
}
// Ripple
export const RIPPLE_DEFAULT_LOAD_FACTOR = 1;
export const RIPPLE_DEFAULT_BASE_FEE = 0.00001;
export const RIPPLE_ALLOWED_CHARS = 'rpshnaf39wBUDNEGHJKLM4PQRST7VWXYZ2bcdeCg65jkm8oFqi1tuvAxyz';
// Stellar
// RFC4648 base-32 encoding
export const STELLAR_BASE32_CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';
export enum STELLAR_VERSION_BYTES {
  ED25519_PUBLIC_KEY = 6 << 3,
  ED25519_SECRET_SEED = 18 << 3,
  MED25519_PUBLIC_KEY = 12 << 3,
  PRE_AUTH_TX = 19 << 3,
  SHA256_HASH = 23 << 3,
  SIGNED_PAYLOAD = 15 << 3,
}

// Units related constants
// COSMOS
export const ONE_ATOM_TO_UATOM = new BigNumber('10e5').toString(10);
// Ethereum
export const ONE_ETH_TO_WEI = new BigNumber('10e18').toString(10);
export const ONE_ETH_TO_GWEI = new BigNumber('10e9').toString(10);
// Ripple
export const ONE_XRP_TO_DROP = new BigNumber('10e5').toString(10);
// Solana
export const ONE_SOL_TO_LAMPORT = new BigNumber('10e9').toString(10);
// Stellar
export const ONE_XLM_TO_STROOP = new BigNumber('10e6').toString(10);

export enum BASE_UNIT {
  RIPPLE = '1000000',
  COSMOS = '1000000',
  SOLANA = '1000000000',
  STELLAR = '10000000',
  POLYGON = '1000000000000000000',
  ETHEREUM = '1000000000000000000',
}

// Ticker related constants
export enum NATIVE_TICKER {
  RIPPLE = 'XRP',
  STELLAR = 'XLM',
  COSMOS = 'ATOM',
  ETHEREUM = 'ETH',
}

// https://github.com/satoshilabs/slips/blob/master/slip-0044.md
export enum DERIVATION_PATH {
  RIPPLE = 'm/44\'/144\'/0\'/0/0',
  SOLANA = 'm/44\'/501\'/0\'/0\'',
  ETHEREUM = 'm/44\'/60\'/0\'/0/0',
  POLYGON = 'm/44\'/966\'/0\'/0/0',
  STELLAR = 'm/44\'/148\'/0\'/0\'',
  COSMOS = 'm/44\'/118\'/0\'/0/0',
}
