import baseX from 'base-x';
import { crc16xmodem } from 'crc';
import sodium from 'sodium-native';
import * as bip39 from '@scure/bip39';
import * as bip32Ed25519 from 'ed25519-hd-key';
import { wordlist } from '@scure/bip39/wordlists/english';

import { StellarWallet } from '../interfaces';
import { toStellarAddress } from '../utils/conversion';
import { DERIVATION_PATH, STELLAR_BASE32_CHARS, STELLAR_VERSION_BYTES } from '../constants';

const encodeGeneratedKey = (keyType: STELLAR_VERSION_BYTES, key: Buffer): string => {
  const payload = Buffer.concat([Buffer.from([keyType]), key]);
  const checksum = Buffer.alloc(2);
  checksum.writeUInt16LE(crc16xmodem(payload), 0);
  const unencoded = Buffer.concat([payload, checksum]);

  return baseX(STELLAR_BASE32_CHARS).encode(unencoded);
}

const createStellarWallet = async (): Promise<StellarWallet> => {
  const mnemonic = bip39.generateMnemonic(wordlist);
  const seed = await bip39.mnemonicToSeed(mnemonic);
  const { key: secretSeed } = bip32Ed25519.derivePath(DERIVATION_PATH.STELLAR, Buffer.from(seed).toString('hex'));
  const unEncodedPublicKey = Buffer.alloc(sodium.crypto_sign_PUBLICKEYBYTES);
  sodium.crypto_sign_seed_keypair(unEncodedPublicKey, Buffer.alloc(sodium.crypto_sign_SECRETKEYBYTES), secretSeed);
  const publicKey = encodeGeneratedKey(STELLAR_VERSION_BYTES.ED25519_PUBLIC_KEY, unEncodedPublicKey);
  const privateKey = encodeGeneratedKey(STELLAR_VERSION_BYTES.ED25519_SECRET_SEED, secretSeed);

  return {
    address: toStellarAddress(publicKey),
    mnemonic,
    publicKey,
    privateKey,
  };
}

export default { stellar: { create: createStellarWallet } };
