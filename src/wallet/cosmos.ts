import { bech32 } from 'bech32';
import * as bip32 from '@scure/bip32';
import * as bip39 from '@scure/bip39';
import { sha256 } from '@noble/hashes/sha256';
import { ripemd160 } from '@noble/hashes/ripemd160';
import { wordlist } from '@scure/bip39/wordlists/english';

import { CosmosWallet } from '../interfaces';
import { DERIVATION_PATH } from '../constants';
import { toCosmosAddress } from '../utils/conversion';

const createCosmosWallet = async (): Promise<CosmosWallet> => {
  const mnemonic = bip39.generateMnemonic(wordlist);
  const seed = await bip39.mnemonicToSeed(mnemonic);
  const { publicKey, privateKey } = bip32.HDKey.fromMasterSeed(seed).derive(DERIVATION_PATH.COSMOS);
  const afterSha256 = sha256.create().update(publicKey as Uint8Array).digest();
  const afterRipemd160 = ripemd160.create().update(afterSha256).digest();
  const address = toCosmosAddress(bech32.encode('cosmos', bech32.toWords(afterRipemd160)));

  return {
    address,
    mnemonic,
    publicKey: Buffer.from(publicKey as Uint8Array).toString('hex'),
    privateKey: Buffer.from(privateKey as Uint8Array).toString('hex'),
  };
}

export default { cosmos: { create: createCosmosWallet } };
