import * as bip39 from '@scure/bip39';
import { Keypair } from '@solana/web3.js';
import * as bip32Ed25519 from 'ed25519-hd-key';
import { wordlist } from '@scure/bip39/wordlists/english';

import { Wallet } from '../interfaces';
import { DERIVATION_PATH } from '../constants';

export const createSolanaWallet = async (): Promise<Wallet> => {
  const mnemonic = bip39.generateMnemonic(wordlist);
  const seed = await bip39.mnemonicToSeed(mnemonic);
  const { key: derivedSeed } = bip32Ed25519.derivePath(DERIVATION_PATH.SOLANA, Buffer.from(seed).toString('hex'));
  const wallet = Keypair.fromSeed(derivedSeed);

  return {
    mnemonic,
    address: wallet.publicKey.toString(),
    publicKey: wallet.publicKey.toString(),
    privateKey: Buffer.from(wallet.secretKey).toString('hex'),
  };
}

export default { solana: { create: createSolanaWallet } };
