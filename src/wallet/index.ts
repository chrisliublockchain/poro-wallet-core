import evmWallet from './evm';
import rippleWallet from './ripple';
import solanaWallet from './solana';
import cosmosWallet from './cosmos';
import stellarWallet from './stellar';

export default {
  ...evmWallet,
  ...rippleWallet,
  ...solanaWallet,
  ...cosmosWallet,
  ...stellarWallet,
}
