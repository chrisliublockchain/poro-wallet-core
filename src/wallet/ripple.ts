import * as bip32 from '@scure/bip32';
import * as bip39 from '@scure/bip39';
import Keypairs from 'ripple-keypairs';
import { wordlist } from '@scure/bip39/wordlists/english';

import { RippleWallet } from '../interfaces';
import { DERIVATION_PATH } from '../constants';
import { toRippleAddress } from '../utils/conversion';

const createRippleWallet = async (): Promise<RippleWallet> => {
  const mnemonic = bip39.generateMnemonic(wordlist);
  const seed = await bip39.mnemonicToSeed(mnemonic);
  const wallet = bip32.HDKey.fromMasterSeed(seed).derive(DERIVATION_PATH.RIPPLE);
  const publicKey = Buffer.from(wallet.publicKey as Uint8Array).toString('hex').toUpperCase();
  const privateKey = `00${Buffer.from(wallet.privateKey as Uint8Array).toString('hex').toUpperCase()}`;
  const address = toRippleAddress(Keypairs.deriveAddress(publicKey));

  return {
    address,
    mnemonic,
    publicKey,
    privateKey,
  };
}

export default { ripple: { create: createRippleWallet } };
