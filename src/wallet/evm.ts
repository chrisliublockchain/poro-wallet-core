import keccak256 from 'keccak256';
import * as bip32 from '@scure/bip32';
import * as bip39 from '@scure/bip39';
import * as secp256k1 from '@noble/secp256k1';
import { wordlist } from '@scure/bip39/wordlists/english';

import { EvmWallet } from '../interfaces';
import { toChecksum } from '../utils/address';
import { DERIVATION_PATH } from '../constants';

const createEvmWallet = async (path: string): Promise<EvmWallet> => {
  const mnemonic = bip39.generateMnemonic(wordlist);
  const seed = await bip39.mnemonicToSeed(mnemonic);
  const wallet = bip32.HDKey.fromMasterSeed(seed).derive(path);
  const privateKey = wallet.privateKey as Uint8Array;
  // Public key can be in either compressed or uncompressed form
  // Compressed: length of 33
  // Uncompressed: length of 65
  const publicKey = Buffer.from(secp256k1.getPublicKey(privateKey)).toString('hex');
  const hash = Buffer.from(keccak256(`0x${publicKey.slice(2)}`)).toString('hex');
  const address = toChecksum(`0x${hash.slice(-40)}`);

  return {
    address,
    mnemonic,
    publicKey: `0x${publicKey}`,
    privateKey: `0x${Buffer.from(privateKey).toString('hex')}`,
  };
}

export default {
  ethereum: { create: async (): Promise<EvmWallet> => await createEvmWallet(DERIVATION_PATH.ETHEREUM) },
  polygon: { create: async (): Promise<EvmWallet> => await createEvmWallet(DERIVATION_PATH.POLYGON) },
}
