import { Balance } from '../interfaces';
import { toRippleAddress } from '../utils/conversion';
import { BASE_UNIT, NATIVE_TICKER } from '../constants';
import { getRippleAccountData, getRippleAccountLines } from '../utils/rpc/ripple';

const getRippleBalance = async (node: string, account: string): Promise<Balance[]> => {
  const verifiedAccount = toRippleAddress(account);
  const nativeTokenBalance = await getRippleAccountData(node, verifiedAccount);
  const accountLines = await getRippleAccountLines(node, verifiedAccount);
  const tokenBalance = accountLines.map((line) => ({
    ticker: line.currency,
    amount: line.balance,
    decimals: '1',
  }));

  return [{
    ticker: NATIVE_TICKER.RIPPLE,
    amount: nativeTokenBalance.Balance,
    decimals: BASE_UNIT.RIPPLE,
  }, ...tokenBalance];
}

export default getRippleBalance;
