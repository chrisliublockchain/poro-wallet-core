/* eslint-disable @typescript-eslint/restrict-template-expressions */
import axios from 'axios';
import BigNumber from 'bignumber.js';

import { toStellarAddress } from '../utils/conversion';
import { BASE_UNIT, NATIVE_TICKER } from '../constants';
import { Balance, StellarAccountInfoResponse } from '../interfaces';

// https://developers.stellar.org/api/resources/accounts/
const getStellarBalance = async (node: string, account: string): Promise<Balance[]> => {
  const url = `${node}/accounts/${toStellarAddress(account)}`;

  try {
    const { data }: { data: StellarAccountInfoResponse } = await axios.get(url, {
      headers: {
        'Content-Type': 'application/json',
      },
    });

    return data.balances.map((balance) => ({
      ticker: typeof balance.asset_code === 'undefined' ? NATIVE_TICKER.STELLAR : balance.asset_code,
      amount: new BigNumber(balance.balance).multipliedBy(BASE_UNIT.STELLAR).toString(),
      decimals: BASE_UNIT.STELLAR,
    }));
  } catch (error: any) {
    throw Error(`Unexpected error encountered when querying balance for account '${account}': ${error.message}`);
  }
}

export default getStellarBalance;
