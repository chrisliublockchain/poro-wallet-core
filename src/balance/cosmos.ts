/* eslint-disable @typescript-eslint/restrict-template-expressions */
import { Coin, StargateClient } from '@cosmjs/stargate';

import { Balance } from '../interfaces';
import { toCosmosAddress } from '../utils/conversion';
import { BASE_UNIT, NATIVE_TICKER } from '../constants';

// https://cosmos.github.io/cosmjs/latest/stargate/classes/StargateClient.html#getBalance
const getCosmosBalance = async (node: string, account: string): Promise<Balance[]> => {
  const verifiedAccount = toCosmosAddress(account);
  try {
    const client = await StargateClient.connect(node);
    const balances = await client.getAllBalances(verifiedAccount) as Coin[];
    await client.disconnect();

    return [{
      ticker: NATIVE_TICKER.COSMOS,
      amount: balances.filter((balance) => balance.denom === 'uatom')[0].amount,
      decimals: BASE_UNIT.COSMOS,
    }];
  } catch (error: any) {
    throw Error(`Unexpected error encountered when querying balance for account '${account}': ${error.message}`);
  }
}

export default getCosmosBalance;
