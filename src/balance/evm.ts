import { ethers } from 'ethers';
import BigNumber from 'bignumber.js';

import { toEvmAddress } from '../utils/conversion';
import { BASE_UNIT, NATIVE_TICKER } from '../constants';
import { Balance, EvmAddress, GetEvmBalanceParams, GetEvmBalanceParamsContract } from '../interfaces';

const getErcTokenBalance = (
  account: EvmAddress,
  contract: GetEvmBalanceParamsContract,
  provider: ethers.providers.JsonRpcProvider,
): [Promise<ethers.BigNumber>, Promise<number>] => {
  const { address: unverifiedAddress, abi } = contract;
  const address = toEvmAddress(unverifiedAddress);
  const ethersContract = new ethers.Contract(address, abi, provider);
  // function 'balanceOf' will generate a ethers.BigNumber type
  const balance = ethersContract.balanceOf(account);
  // function 'decimals' will generate a number type
  const decimals = ethersContract.decimals();
  return [balance, decimals];
}

const getEvmBalance = async (params: GetEvmBalanceParams): Promise<Balance[]> => {
  const { node, account: unverifiedAccount, contracts } = params;
  const account = toEvmAddress(unverifiedAccount);
  const provider = new ethers.providers.JsonRpcBatchProvider(node);

  const promises = [
    provider.getBalance(account),
    ...contracts
      .map((contract) => getErcTokenBalance(account, contract, provider))
      .reduce((prev: any[], curr) => prev.concat(curr), []),
  ];

  const [nativeTokenBalance, ...tokenBalanceDetails] = await Promise.all(promises);

  const tokenBalances: Balance[] = [];
  for (let i = 0; i < tokenBalanceDetails.length; i += 2) {
    tokenBalances.push({
      ticker: contracts[Math.floor(i / 2)].ticker,
      amount: tokenBalanceDetails[i].toString(),
      decimals: new BigNumber(10).pow(tokenBalanceDetails[i + 1]).toString(),
    });
  }

  return [{
    ticker: NATIVE_TICKER.ETHEREUM,
    amount: nativeTokenBalance.toString(),
    decimals: BASE_UNIT.ETHEREUM,
  }, ...tokenBalances];
}

export default getEvmBalance;
