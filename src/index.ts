import price from './price';
import wallet from './wallet';
import faucet from './faucet';
import balance from './balance';
import transaction from './transaction';

import * as utils from './utils';
import * as constants from './constants';

export * from './interfaces';

export default {
  utils,
  price,
  faucet,
  balance,
  factory: {
    wallet,
    transaction,
  },
  constants,
};
