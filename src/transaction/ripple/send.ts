/* eslint-disable @typescript-eslint/restrict-template-expressions */
import * as xrpl from 'xrpl';

export default async (nodeWss: string, signedTx: string): Promise<string> => {
  try {
    const client = new xrpl.Client(nodeWss);

    await client.connect();
    const response = await client.submit(signedTx);
    await client.disconnect();

    if (response.result.accepted) {
      return String(response.result.tx_json.hash);
    }

    throw Error('Transaction rejected');
  } catch (error: any) {
    throw Error(`Cannot send transaction: ${error.message}`);
  }
}
