import BigNumber from 'bignumber.js';

import price from '../../../price';
import { BASE_UNIT } from '../../../constants';
import { toRippleAddress } from '../../../utils/conversion';
import { getRippleAccountData } from '../../../utils/rpc/ripple';
import { RipplePayment, RippleTokenTransactionParams } from '../../../interfaces';

export default async (node: string, params: RippleTokenTransactionParams): Promise<RipplePayment> => {
  const { from, to, amount, currency, contractAddress } = params;

  const Account = toRippleAddress(from);
  const Destination = toRippleAddress(to);
  const fee = await price.ripple.fee(node);
  const issuer = toRippleAddress(contractAddress);
  const { Sequence, Flags } = await getRippleAccountData(node, Account);

  const payment = {
    Flags,
    Account,
    Sequence,
    Destination,
    Amount: {
      issuer,
      currency,
      value: amount,
    },
    TransactionType: 'Payment',
    Fee: new BigNumber(fee).multipliedBy(BASE_UNIT.RIPPLE).toString(),
  };

  return payment;
};
