import { BigNumber } from 'bignumber.js';

import price from '../../../price';
import { BASE_UNIT } from '../../../constants';
import { toRippleAddress } from '../../../utils/conversion';
import { getRippleAccountData } from '../../../utils/rpc/ripple';
import { RippleOfferCreate, RippleTokenOfferCreateTransactionParams } from '../../../interfaces';

export default async (node: string, params: RippleTokenOfferCreateTransactionParams): Promise<RippleOfferCreate> => {
  const { from, to, amount, currency } = params;

  const issuer = toRippleAddress(to);
  const Account = toRippleAddress(from);
  const fee = await price.ripple.fee(node);
  const { Sequence, Flags } = await getRippleAccountData(node, Account);
  const exchangeRate = await price.ripple.token(node, currency, issuer);

  const offerCreate = {
    TransactionType: 'OfferCreate',
    Account,
    TakerGets: new BigNumber(amount)
      .multipliedBy(exchangeRate)
      .multipliedBy(BASE_UNIT.RIPPLE)
      .toFixed(0),
    TakerPays: {
      issuer,
      currency,
      value: amount,
    },
    Fee: new BigNumber(fee).multipliedBy(BASE_UNIT.RIPPLE).toString(),
    Sequence,
    Flags,
  }

  return offerCreate;
}
