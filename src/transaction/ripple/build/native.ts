import BigNumber from 'bignumber.js';

import price from '../../../price';
import { BASE_UNIT } from '../../../constants';
import { toRippleAddress } from '../../../utils/conversion';
import { getRippleAccountData } from '../../../utils/rpc/ripple';
import { RippleNativeTransactionParams, RipplePayment } from '../../../interfaces';

export default async (node: string, params: RippleNativeTransactionParams): Promise<RipplePayment> => {
  const { from, to, amount } = params;
  const Account = toRippleAddress(from);
  const Destination = toRippleAddress(to);
  const fee = await price.ripple.fee(node);
  const { Sequence, Flags } = await getRippleAccountData(node, Account);

  const payment = {
    Flags,
    Account,
    Sequence,
    Destination,
    TransactionType: 'Payment',
    Fee: new BigNumber(fee).multipliedBy(BASE_UNIT.RIPPLE).toString(),
    Amount: new BigNumber(amount).multipliedBy(BASE_UNIT.RIPPLE).toString(),
  };

  return payment;
}
