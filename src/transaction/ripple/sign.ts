import * as xrpl from 'xrpl';

import { RippleBuiltTx } from '../../interfaces';

export default (mnemonic: string, transaction: RippleBuiltTx): string => {
  const wallet = xrpl.Wallet.fromMnemonic(mnemonic);
  const { tx_blob: signedTx } = wallet.sign(transaction as xrpl.Transaction);
  return signedTx;
}
