import { SigningStargateClient } from '@cosmjs/stargate';
import { TxRaw } from 'cosmjs-types/cosmos/tx/v1beta1/tx';
import { DirectSecp256k1HdWallet } from '@cosmjs/proto-signing';

import { CosmosBuiltTx } from '../../interfaces';
import { getCosmosAccountData, getCosmosChainId } from '../../utils/rpc/cosmos';
import { COSMOS_DEFAULT_GAS, COSMOS_DEFAULT_GAS_LIMIT } from '../../constants';

export default async (node: string, mnemonic: string, transaction: CosmosBuiltTx): Promise<string> => {
  const { value: { fromAddress } } = transaction;
  const wallet = await DirectSecp256k1HdWallet.fromMnemonic(mnemonic);
  const signingClient = await SigningStargateClient.offline(wallet);
  const accountData = await getCosmosAccountData(node, fromAddress);
  const chainId = await getCosmosChainId(node);

  const txRaw = await signingClient.sign(
    transaction.value.fromAddress,
    [transaction],
    {
      amount: [{
        denom: 'uatom',
        amount: String(COSMOS_DEFAULT_GAS),
      }],
      gas: String(COSMOS_DEFAULT_GAS_LIMIT),
    },
    '',
    {
      accountNumber: accountData.accountNumber,
      sequence: accountData.sequence,
      chainId,
    },
  );

  const signedTx = Buffer.from(TxRaw.encode(txRaw).finish()).toString('hex');
  return signedTx;
}
