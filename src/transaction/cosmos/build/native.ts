import BigNumber from 'bignumber.js';

import { BASE_UNIT } from '../../../constants';
import { toCosmosAddress } from '../../../utils/conversion';
import { CosmosBankMessageSendParams, CosmosBuiltTx } from '../../../interfaces';

export default (params: CosmosBankMessageSendParams): CosmosBuiltTx => {
  const { from, to, amount } = params;
  const toAddress = toCosmosAddress(to);
  const fromAddress = toCosmosAddress(from);

  const message: CosmosBuiltTx = {
    typeUrl: '/cosmos.bank.v1beta1.MsgSend',
    value: {
      fromAddress,
      toAddress,
      amount: [{
        denom: 'uatom',
        amount: new BigNumber(amount).multipliedBy(BASE_UNIT.COSMOS).toString(),
      }],
    },
  };

  return message;
}
