import { StargateClient } from '@cosmjs/stargate';

export default async (node: string, signedTx: string): Promise<boolean> => {
  const client = await StargateClient.connect(node);
  const result = await client.broadcastTx(Buffer.from(signedTx, 'hex'));
  return result.code === 0;
}
