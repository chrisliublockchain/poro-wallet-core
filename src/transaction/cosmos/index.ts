/* eslint-disable import/prefer-default-export */
import sign from './sign';
import send from './send';
import native from './build/native';

export const cosmos = {
  sign,
  send,
  native,
};
