/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
import { providers } from 'ethers';

import { TransactionStatus } from '../../interfaces';

export default async (node: string, hashes: string[]): Promise<{ [hash: string]: string }> => {
  const provider = new providers.JsonRpcBatchProvider(node);

  const receipts: { [hash: string]: string } = {};

  for (const hash of hashes) {
    try {
      const receipt = await provider.getTransactionReceipt(hash);
      if (typeof receipt.status !== 'undefined' && receipt.status === 1) {
        receipts[hash] = TransactionStatus.Accepted;
      } else {
        receipts[hash] = TransactionStatus.Rejected;
      }
    } catch (error: any) {
      receipts[hash] = TransactionStatus.Pending;
    }
  }

  return receipts;
}
