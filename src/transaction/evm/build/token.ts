/* eslint-disable import/prefer-default-export */
import BigNumber from 'bignumber.js';

import {
  EvmBuiltTx,
  TransferFunctionParams,
  EvmTokenTransactionParams,
  Erc20TransferFunctionParams,
} from '../../../interfaces';
import { toEvmAddress } from '../../../utils/conversion';
import { encodeData, prepareNodeInformation } from '../utils';
import { ABI_ERC20, ETHEREUM_DEFAULT_GAS_LIMIT } from '../../../constants';

const token = <T extends TransferFunctionParams>(encoder: (inputs: string[]) => string) => async (
  node: string,
  params: EvmTokenTransactionParams<T>,
): Promise<EvmBuiltTx> => {
  const { from, contractAddress, functionParams } = params;
  const to = toEvmAddress(contractAddress);
  const fromAddress = toEvmAddress(from);
  const { chainId, nonce, maxFeePerGas, maxPriorityFeePerGas } = await prepareNodeInformation(node, fromAddress);

  const tx = {
    to,
    chainId,
    // EIP-1559 transaction
    type: 2,
    value: '0x0',
    maxFeePerGas,
    maxPriorityFeePerGas,
    data: encoder(Object.values(functionParams)),
    nonce: `0x${new BigNumber(nonce).toString(16)}`,
    gasLimit: `0x${new BigNumber(ETHEREUM_DEFAULT_GAS_LIMIT).toString(16)}`,
  };

  return tx;
};

export const erc20Token = token<Erc20TransferFunctionParams>(encodeData(ABI_ERC20, 'transfer'));
