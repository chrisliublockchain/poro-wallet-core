import BigNumber from 'bignumber.js';

import { prepareNodeInformation } from '../utils';
import { toEvmAddress } from '../../../utils/conversion';
import { ETHEREUM_DEFAULT_GAS_LIMIT } from '../../../constants';
import { EvmBuiltTx, EvmNativeTransactionParams } from '../../../interfaces';

export default async (node: string, params: EvmNativeTransactionParams): Promise<EvmBuiltTx> => {
  const { from, to, amount: value } = params;
  const toAddress = toEvmAddress(to);
  const fromAddress = toEvmAddress(from);
  const { chainId, nonce, maxFeePerGas, maxPriorityFeePerGas } = await prepareNodeInformation(node, fromAddress);

  const tx = {
    value,
    chainId,
    // EIP-1559 transaction
    type: 2,
    maxFeePerGas,
    to: toAddress,
    maxPriorityFeePerGas,
    nonce: `0x${new BigNumber(nonce).toString(16)}`,
    gasLimit: `0x${new BigNumber(ETHEREUM_DEFAULT_GAS_LIMIT).toString(16)}`,
  };

  return tx;
}
