/* eslint-disable import/prefer-default-export */
import sign from './sign';
import send from './send';
import status from './status';
import native from './build/native';
import { erc20Token } from './build/token';

export const evm = {
  sign,
  send,
  status,
  native,
  token: {
    erc20: erc20Token,
  },
};
