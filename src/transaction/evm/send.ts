/* eslint-disable @typescript-eslint/restrict-template-expressions */
import { providers } from 'ethers';

export default async (node: string, signedTx: string): Promise<string> => {
  try {
    const provider = new providers.JsonRpcProvider(node);
    const { hash } = await provider.sendTransaction(signedTx);
    return hash;
  } catch (error: any) {
    throw Error(`Cannot send transaction: ${error.message}`);
  }
}
