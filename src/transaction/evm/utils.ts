import { providers, utils } from 'ethers';
import { JsonFragment } from '@ethersproject/abi';

export const prepareNodeInformation = async (
  node: string,
  address: string,
): Promise<{
  nonce: number
  chainId: number
  maxFeePerGas: string
  maxPriorityFeePerGas: string
}> => {
  const provider = new providers.JsonRpcBatchProvider(node);

  const [feeData, nonce, network] = await Promise.all([
    provider.getFeeData(),
    provider.getTransactionCount(address),
    provider.getNetwork(),
  ]);

  if (feeData.maxFeePerGas === null || feeData.maxPriorityFeePerGas === null) {
    throw Error('Cannot get fee data while constructing native transaction');
  }

  const { chainId } = network;
  const { maxFeePerGas, maxPriorityFeePerGas } = feeData;

  return {
    nonce,
    chainId,
    maxFeePerGas: maxFeePerGas.toHexString(),
    maxPriorityFeePerGas: maxPriorityFeePerGas.toHexString(),
  };
}

export const encodeData = (abi: JsonFragment[], functionName: string) => (inputs: string[]): string => {
  const abiInterface = new utils.Interface(abi);
  return abiInterface.encodeFunctionData(functionName, inputs);
}
