import { ethers } from 'ethers';

import { EvmBuiltTx } from '../../interfaces';

export default async (privateKey: string, transaction: EvmBuiltTx): Promise<string> => {
  const wallet = new ethers.Wallet(privateKey);
  const signedTx = await wallet.signTransaction(transaction);
  return signedTx;
}
