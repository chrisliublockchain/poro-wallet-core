import { evm } from './evm';
import { cosmos } from './cosmos';
import { ripple } from './ripple';

export default {
  evm,
  cosmos,
  ripple,
}
