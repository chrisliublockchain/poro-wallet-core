/* eslint-disable @typescript-eslint/restrict-template-expressions */
import axios from 'axios';

import { FaucetResponse } from '../interfaces';
import { toRippleAddress } from '../utils/conversion';

const getFundFromRippleFaucet = async (account: string): Promise<FaucetResponse> => {
  const verifiedAccount = toRippleAddress(account);
  try {
    await axios.post(
      'https://faucet.altnet.rippletest.net:443/accounts',
      { destination: verifiedAccount },
      { headers: { 'Content-Type': 'application/json' } },
    );

    return { success: true, timestamp: Math.floor((new Date()).getTime() / 1000) };
  } catch (error: any) {
    return {
      success: false,
      message: `Unexpected error encountered when requesting fund for account '${account}': ${error.message}`,
    };
  }
}

export default getFundFromRippleFaucet;
