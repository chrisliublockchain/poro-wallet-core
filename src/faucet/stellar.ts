/* eslint-disable @typescript-eslint/restrict-template-expressions */
import axios from 'axios';

import { FaucetResponse } from '../interfaces';
import { toStellarAddress } from '../utils/conversion';

const getFundFromStellarFaucet = async (account: string): Promise<FaucetResponse> => {
  const verifiedAccount = toStellarAddress(account);
  try {
    await axios(`https://friendbot.stellar.org?addr=${verifiedAccount}`);
    return { success: true, timestamp: Math.floor((new Date()).getTime() / 1000) };
  } catch (error: any) {
    return {
      success: false,
      message: `Unexpected error encountered when requesting fund for account '${account}': ${error.message}`,
    };
  }
}

export default getFundFromStellarFaucet;
