import BigNumber from 'bignumber.js';

import { CosmosAddress, EvmAddress, HexString, RippleAddress, StellarAddress } from '../interfaces';
import { isCosmosAddress, isEvmAddress, isHexString, isRippleAddress, isStellarAddress } from './validation';

export const toHexString = (input: string | number): HexString => {
  const hexString = `0x${new BigNumber(String(input)).toString(16)}`;
  if (isHexString(hexString)) return hexString;
  throw Error(`'${input}' is not a valid HexString`);
}

export const toEvmAddress = (input: string): EvmAddress => {
  if (isEvmAddress(input)) return input;
  throw Error(`'${input}' is not a valid EvmAddress`);
}

export const toRippleAddress = (input: string): RippleAddress => {
  if (isRippleAddress(input)) return input;
  throw Error(`'${input}' is not a valid RippleAddress`);
}

export const toStellarAddress = (input: string): StellarAddress => {
  if (isStellarAddress(input)) return input;
  throw Error(`'${input}' is not a valid StellarAddress`);
}

export const toCosmosAddress = (input: string): CosmosAddress => {
  if (isCosmosAddress(input)) return input;
  throw Error(`'${input}' is not a valid CosmosAddress`);
}
