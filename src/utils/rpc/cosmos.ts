import { Account, StargateClient } from '@cosmjs/stargate';

import { toCosmosAddress } from '../conversion';

export const getCosmosAccountData = async (node: string, account: string): Promise<Account> => {
  const verifiedAccount = toCosmosAddress(account);
  const client = await StargateClient.connect(node);
  const targetAccount = await client.getAccount(verifiedAccount);
  if (targetAccount === null) {
    throw Error(`Unexpected error encountered when querying account data for account '${account}'`);
  }
  return targetAccount;
}

export const getCosmosChainId = async (node: string): Promise<string> => {
  const client = await StargateClient.connect(node);
  const chainId = await client.getChainId();
  await client.disconnect();
  return chainId;
}
