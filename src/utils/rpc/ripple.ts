/* eslint-disable @typescript-eslint/restrict-template-expressions */
import axios from 'axios';

import {
  RippleAccountData,
  RippleAccountLine,
  RippleAccountInfoResponse,
  RippleAccountLinesResponse,
} from '../../interfaces';
import { toRippleAddress } from '../conversion';

// https://xrpl.org/account_info.html
export const getRippleAccountData = async (node: string, account: string): Promise<RippleAccountData> => {
  const verifiedAccount = toRippleAddress(account);
  try {
    const { data }: { data: RippleAccountInfoResponse } = await axios.get(node, {
      headers: { 'Content-Type': 'application/json' },
      data: {
        method: 'account_info',
        params: [{ account: verifiedAccount, strict: true }],
      },
    });

    if (data.result.status === 'success' && typeof data.result.account_data !== 'undefined') {
      return data.result.account_data;
    }

    throw Error(data.result.error_message);
  } catch (error: any) {
    throw Error(`Unexpected error encountered when querying account data for account '${account}': ${error.message}`);
  }
}

// https://xrpl.org/account_lines.html
export const getRippleAccountLines = async (node: string, account: string): Promise<RippleAccountLine[]> => {
  const verifiedAccount = toRippleAddress(account);
  try {
    const { data }: { data: RippleAccountLinesResponse } = await axios.get(node, {
      headers: { 'Content-Type': 'application/json' },
      data: {
        method: 'account_lines',
        params: [{ account: verifiedAccount }],
      },
    });

    if (data.result.status === 'success' && typeof data.result.lines !== 'undefined') {
      return data.result.lines;
    }

    throw Error(data.result.error_message);
  } catch (error: any) {
    throw Error(`Unexpected error encountered when querying account lines for account '${account}': ${error.message}`);
  }
}
