/* eslint-disable import/prefer-default-export */
import keccak256 from 'keccak256';

import { EvmAddress } from '../interfaces';
import { toEvmAddress } from './conversion';

export const toChecksum = (address: string): EvmAddress => {
  const chars = toEvmAddress(address).toLowerCase().slice(2).split('');

  const expanded = new Uint8Array(40);
  for (let i = 0; i < 40; i += 1) {
    expanded[i] = chars[i].charCodeAt(0);
  }

  const hashed = keccak256(`0x${Buffer.from(expanded).toString('hex')}`);

  for (let i = 0; i < 40; i += 2) {
    if ((hashed[i >> 1] >> 4) >= 8) {
      chars[i] = chars[i].toUpperCase();
    }
    if ((hashed[i >> 1] & 0x0f) >= 8) {
      chars[i + 1] = chars[i + 1].toUpperCase();
    }
  }

  return toEvmAddress(`0x${chars.join('')}`);
}
