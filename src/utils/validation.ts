import baseX from 'base-x';
import { crc16xmodem } from 'crc';
import { sha256 } from '@noble/hashes/sha256';

import { RIPPLE_ALLOWED_CHARS, STELLAR_BASE32_CHARS } from '../constants';
import { CosmosAddress, EvmAddress, HexString, RippleAddress, StellarAddress } from '../interfaces';

export const isHexString = (input: string): input is HexString => /^0x[0-9a-fA-F]+$/.test(input);

export const isEvmAddress = (input: string): input is EvmAddress => input.length === 42 && /^0x[0-9a-fA-F]+$/.test(input);

export const isRippleAddress = (input: string): input is RippleAddress => {
  if (!new RegExp(`^r[${RIPPLE_ALLOWED_CHARS}]{27,35}$`).test(input)) return false;

  const decoded = baseX(RIPPLE_ALLOWED_CHARS).decode(input);
  const computedChecksum = Buffer.from(sha256(sha256(decoded.slice(0, -4)))).toString('hex').slice(0, 8);
  const checksum = Buffer.from(decoded.slice(-4)).toString('hex');

  return checksum === computedChecksum;
}

export const isStellarAddress = (input: string): input is StellarAddress => {
  if (input.length !== 56) return false;

  const base32Decoded = baseX(STELLAR_BASE32_CHARS).decode(input);

  if (input !== baseX(STELLAR_BASE32_CHARS).encode(base32Decoded)) return false;

  const payload = base32Decoded.slice(0, -2);
  const data = payload.slice(1);
  const checksum = base32Decoded.slice(-2);
  const expectedChecksum = Buffer.alloc(2);
  expectedChecksum.writeUInt16LE(crc16xmodem(payload), 0);

  if (expectedChecksum.length !== checksum.length) return false;

  for (let i = 0; i < expectedChecksum.length; i += 1) {
    if (expectedChecksum[i] !== checksum[i]) return false;
  }

  return data.length === 32;
}

export const isCosmosAddress = (input: string): input is CosmosAddress => input.length === 45 && input.startsWith('cosmos');
