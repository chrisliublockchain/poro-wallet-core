import BigNumber from 'bignumber.js';

import core from '../../src';
import wallets from '../wallet.json';

describe('[transaction][evm]', () => {
  const node = String(process.env.ETHEREUM_GOERLI_NODE_URL);
  const randomTo = '0xf97e180c050e5Ab072211Ad2C213Eb5AEE4DF134';
  const uniswapContractAddress = '0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984';

  test('native() - Incorrect node url', async () => {
    const { address } = wallets.ethereum;
    await expect(core.factory.transaction.evm.native(
      'https://test.test',
      { from: address, to: randomTo, amount: '0.0000001' },
    ))
      .rejects
      .toThrow('could not detect network (event="noNetwork", code=NETWORK_ERROR, version=providers/5.7.0)');
  });

  test('native() - Successfully generate builtTx', async () => {
    const { address: from } = wallets.ethereum;
    const builtTx = await core.factory.transaction.evm.native(
      node,
      {
        from,
        to: randomTo,
        amount: `0x${new BigNumber('0.0000001').multipliedBy(core.constants.BASE_UNIT.ETHEREUM).toString(16)}`,
      },
    );

    expect(typeof builtTx).not.toBe('undefined');
    expect(builtTx.to).toBe(randomTo);
    expect(builtTx.value).toBe('0x174876e800');
    expect(builtTx.chainId).toBe(5);
    expect(builtTx.type).toBe(2);
  });

  test('sendErc20Token() - Successfully generate builtTx', async () => {
    const { address: from } = wallets.ethereum;
    const builtTx = await core.factory.transaction.evm.token.erc20(
      node,
      {
        from,
        contractAddress: uniswapContractAddress,
        functionParams: {
          to: from,
          value: '0x1',
        }
      }
    );

    expect(typeof builtTx).not.toBe('undefined');
    expect(builtTx.to).toBe(uniswapContractAddress);
    expect(builtTx.chainId).toBe(5);
    expect(builtTx.type).toBe(2);
    expect(builtTx.data).toBe('0xa9059cbb00000000000000000000000082c6d065e59fa161c8e5a6dd90e2431df90830f30000000000000000000000000000000000000000000000000000000000000001');
  });

  test('status() - ', async () => {
    const hashes = [
      '0x0c80982e944aec4db3ab3a580f71fb9a826db91d364e7e8b83c2143a622dde4c',
      '0xf4d6f767119ae197bd4fb17d7c5227781572ac4f5c8cbebd4a471e44bd652364',
      '0xb80242e0cbe028f89396fb944dc93dca87d92d355e0ff28b52bdfd548c348ae5',
    ];
    const status = await core.factory.transaction.evm.status(node, hashes);

    expect(status[hashes[0]]).toBe('Rejected');
    expect(status[hashes[1]]).toBe('Rejected');
    expect(status[hashes[2]]).toBe('Accepted');
  })

  xtest('full cycle of native transaction', async () => {
    const { address, privateKey } = wallets.ethereum;
    const builtTx = await core.factory.transaction.evm.native(
      node,
      { from: address, to: randomTo, amount: '0.0000001' },
    );
    const signedTx = await core.factory.transaction.evm.sign(privateKey, builtTx);
    const sentResponse = await core.factory.transaction.evm.send(node, signedTx);

    expect(typeof sentResponse).toBe('string');
  });
});
