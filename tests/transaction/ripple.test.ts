import * as xrpl from 'xrpl';

import wallets from '../wallet.json';
import core, { RippleAmount } from '../../src';

describe('[transaction][ripple]', () => {
  const node = String(process.env.RIPPLE_NODE_URL);
  const nodeWss = String(process.env.RIPPLE_WSS_NODE_URL);
  const randomTo = 'rnfxSqq2jKeaY4XoZojqjuCWVrGKFxrk8Y';

  test('native() - Successfully generate builtTx', async () => {
    const { address: from } = wallets.ripple;
    const builtTx = await core.factory.transaction.ripple.native(
      node,
      { from, to: randomTo, amount: '0.001' }
    );

    expect(typeof builtTx).not.toBe('undefined');
    expect(builtTx.Account).toBe(from);
    expect(builtTx.Destination).toBe(randomTo);
    expect(builtTx.TransactionType).toBe('Payment');
    expect(builtTx.Amount).toBe('1000');
  });

  test('token() - Successfully generate builtTx', async () => {
    const { address: from } = wallets.ripple;
    const currency = 'USD';
    const amount = '0.001';
    const contractAddress = 'rBZJzEisyXt2gvRWXLxHftFRkd1vJEpBQP';
    const builtTx = await core.factory.transaction.ripple.token(
      node,
      {
        from,
        amount,
        currency,
        to: randomTo,
        contractAddress,
      }
    );

    expect(typeof builtTx).not.toBe('undefined');
    expect(builtTx.Account).toBe(from);
    expect(builtTx.Destination).toBe(randomTo);
    expect(builtTx.TransactionType).toBe('Payment');
    expect((builtTx.Amount as RippleAmount).value).toBe(amount);
    expect((builtTx.Amount as RippleAmount).currency).toBe(currency);
    expect((builtTx.Amount as RippleAmount).issuer).toBe(contractAddress);
  });

  test('createTokenOffer() - Successfully generate builtTx', async () => {
    const { address: from } = wallets.ripple;
    const currency = 'USD';
    const amount = '0.001';
    const contractAddress = 'rBZJzEisyXt2gvRWXLxHftFRkd1vJEpBQP';
    const builtTx = await core.factory.transaction.ripple.createTokenOffer(
      node,
      { from, currency, amount, to: contractAddress },
    );

    expect(typeof builtTx).not.toBe('undefined');
    expect(builtTx.Account).toBe(from);
    expect(typeof builtTx.TakerGets).toBe('string');
    expect(builtTx.TransactionType).toBe('OfferCreate');
    expect((builtTx.TakerPays as RippleAmount).value).toBe(amount);
    expect((builtTx.TakerPays as RippleAmount).currency).toBe(currency);
    expect((builtTx.TakerPays as RippleAmount).issuer).toBe(contractAddress);
  });

  xtest('full cycle of payment tx', async () => {
    const wallet = await core.factory.wallet.ripple.create();
    const client = new xrpl.Client(nodeWss);
    await client.connect();
    await client.fundWallet(xrpl.Wallet.fromMnemonic(wallet.mnemonic));
    await client.disconnect();
    const builtTx = await core.factory.transaction.ripple.native(
      node,
      {
        from: wallet.address,
        to: wallets.ripple.address,
        amount: '10000000',
      },
    );
    const signedTx = core.factory.transaction.ripple.sign(wallet.mnemonic, builtTx);
    const sentResponse = await core.factory.transaction.ripple.send(nodeWss, signedTx);

    expect(typeof sentResponse).toBe('string');
  });

  xtest('full cycle of token payment tx', async () => {
    const { address, mnemonic } = wallets.ripple;
    const currency = 'USD';
    const contractAddress = 'rBZJzEisyXt2gvRWXLxHftFRkd1vJEpBQP';
    const builtTx = await core.factory.transaction.ripple.token(
      node,
      {
        from: address,
        to: contractAddress,
        amount: '0.00148724',
        currency,
        contractAddress,
      },
    );

    const signedTx = core.factory.transaction.ripple.sign(mnemonic, builtTx);
    const sentResponse = await core.factory.transaction.ripple.send(nodeWss, signedTx);

    expect(typeof sentResponse).toBe('string');
  });

  xtest('full cycle of offer create tx (token())', async () => {
    const { address, mnemonic } = wallets.ripple;
    const currency = 'USD';
    const contractAddress = 'rBZJzEisyXt2gvRWXLxHftFRkd1vJEpBQP';
    const builtTx = await core.factory.transaction.ripple.createTokenOffer(
      node,
      {
        from: address,
        to: contractAddress,
        amount: '1',
        currency,
      },
    );

    const signedTx = core.factory.transaction.ripple.sign(mnemonic, builtTx);
    const sentResponse = await core.factory.transaction.ripple.send(nodeWss, signedTx);

    expect(typeof sentResponse).toBe('string');
  });
});
