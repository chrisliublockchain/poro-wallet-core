import core from '../../src';
import wallets from '../wallet.json';

describe('[transaction][cosmos]', () => {
  const node = String(process.env.COSMOS_NODE_URL);
  const randomTo = 'cosmos1j4zgxxzg7jgevmckaty3pf32kxdh9edk4q8tg2';

  xtest('full cycle of bank message send tx', async () => {
    const { address, mnemonic } = wallets.cosmos;
    const builtTx = core.factory.transaction.cosmos.native({ from: address, to: randomTo, amount: '0.001' });
    const signedTx = await core.factory.transaction.cosmos.sign(node, mnemonic, builtTx);
    const sentResponse = await core.factory.transaction.cosmos.send(node, signedTx);

    expect(sentResponse).toBe(true);
  });
});
