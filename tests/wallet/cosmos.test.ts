import { DirectSecp256k1HdWallet } from '@cosmjs/proto-signing';

import core from '../../src';

describe('[wallet][cosmos]', () => {  
  test('create()', async () => {
    const wallet = await core.factory.wallet.cosmos.create();
    const recoveredWallet: DirectSecp256k1HdWallet = await DirectSecp256k1HdWallet.fromMnemonic(wallet.mnemonic);
    const accounts = await recoveredWallet.getAccounts();

    expect(wallet.address).toBe(accounts[0].address);
    expect(wallet.publicKey).toBe(Buffer.from(accounts[0].pubkey).toString('hex'));
  });
});
