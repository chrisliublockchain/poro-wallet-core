import { Keypair as SolanaKeypair } from '@solana/web3.js';

import core from '../../src';

describe('[wallet][solana]', () => {  
  test('create()', async () => {
    const wallet = await core.factory.wallet.solana.create();
    const recoveredWallet = SolanaKeypair.fromSecretKey(Buffer.from(wallet.privateKey, 'hex'));

    expect(wallet.publicKey).toBe(recoveredWallet.publicKey.toBase58());
    expect(wallet.privateKey).toBe(Buffer.from(recoveredWallet.secretKey).toString('hex'));
  });
});
