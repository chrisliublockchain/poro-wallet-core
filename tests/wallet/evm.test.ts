import { ethers } from 'ethers';

import core from '../../src';

describe('[wallet][evm]', () => {
  test('ethereum.create()', async () => {
    const wallet = await core.factory.wallet.ethereum.create();
    const recoveredWallet = ethers.Wallet.fromMnemonic(wallet.mnemonic, core.constants.DERIVATION_PATH.ETHEREUM);

    expect(wallet.address).toBe(recoveredWallet.address);
    expect(wallet.publicKey).toBe(recoveredWallet.publicKey);
    expect(wallet.privateKey).toBe(recoveredWallet.privateKey);
    expect(wallet.mnemonic).toBe(recoveredWallet.mnemonic.phrase);
  });

  test('polygon.create()', async () => {
    const wallet = await core.factory.wallet.polygon.create();
    const recoveredWallet = ethers.Wallet.fromMnemonic(wallet.mnemonic, core.constants.DERIVATION_PATH.POLYGON);

    expect(wallet.address).toBe(recoveredWallet.address);
    expect(wallet.publicKey).toBe(recoveredWallet.publicKey);
    expect(wallet.privateKey).toBe(recoveredWallet.privateKey);
    expect(wallet.mnemonic).toBe(recoveredWallet.mnemonic.phrase);
  });
});
