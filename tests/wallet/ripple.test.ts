import * as xrpl from 'xrpl';

import core from '../../src';

describe('[wallet][ripple]', () => {
  test('create()', async () => {
    const wallet = await core.factory.wallet.ripple.create();
    const recoveredWallet = xrpl.Wallet.fromMnemonic(wallet.mnemonic);

    expect(wallet.address).toBe(recoveredWallet.address);
    expect(wallet.publicKey).toBe(recoveredWallet.publicKey);
    expect(wallet.privateKey).toBe(recoveredWallet.privateKey);
  });
});
