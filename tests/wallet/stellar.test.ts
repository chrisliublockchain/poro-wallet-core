import { Keypair as StellarKeypair } from 'stellar-sdk';

import core from '../../src';

describe('[wallet][stellar]', () => {  
  test('create()', async () => {
    const wallet = await core.factory.wallet.stellar.create();
    const recoveredWallet = StellarKeypair.fromSecret(wallet.privateKey);

    expect(wallet.address).toBe(recoveredWallet.publicKey())
    expect(wallet.publicKey).toBe(recoveredWallet.publicKey());
    expect(wallet.privateKey).toBe(recoveredWallet.secret());
  });
});
