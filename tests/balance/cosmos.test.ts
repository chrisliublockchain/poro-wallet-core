import core from '../../src';

describe('[balance][cosmos]', () => {
  const node = String(process.env.COSMOS_NODE_URL);
  const account = 'cosmos1m7uyxn26sz6w4755k6rch4dc2fj6cmzajkszvn';

  test('getCosmosBalance() - Success', async () => {
    const balance = await core.balance.cosmos(node, account);

    expect(balance.length).toBeGreaterThanOrEqual(0);
    expect(balance[0].decimals).toBe('1000000');
  });

  test('getCosmosBalance() - Incorrect node url', async () => {
    await expect(core.balance.cosmos('https://test.test', account))
      .rejects
      .toThrow(`Unexpected error encountered when querying balance for account 'cosmos1m7uyxn26sz6w4755k6rch4dc2fj6cmzajkszvn': getaddrinfo ENOTFOUND test.test`);
  });
});
