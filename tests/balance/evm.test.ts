import core from '../../src';
import wallets from '../wallet.json';

describe('[balance][evm]', () => {
  const account = wallets.ethereum.address;
  const node = String(process.env.ETHEREUM_GOERLI_NODE_URL);

  test('getEvmBalance() - Success', async () => {
    const balance = await core.balance.evm({
      node,
      account: account,
      contracts: [
        { ticker: 'UNI', address: '0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984', abi: core.constants.ABI_ERC20 },
        { ticker: 'LINK', address: '0x326C977E6efc84E512bB9C30f76E30c160eD06FB', abi: core.constants.ABI_ERC20 },
        { ticker: 'WETH', address: '0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6', abi: core.constants.ABI_ERC20 },
      ],
    });

    expect(balance.length).toBeGreaterThanOrEqual(0);
    expect(balance[0].ticker).toBe('ETH');
  });

  test('getEvmBalance() - Incorrect node url', async () => {
    await expect(core.balance.evm({
      node: 'https://test.test',
      account: account,
      contracts: [],
    }))
      .rejects
      .toThrow('could not detect network (event=\"noNetwork\", code=NETWORK_ERROR, version=providers/5.7.0)');;
  });
});
