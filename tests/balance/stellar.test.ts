import core from '../../src';

describe('[balance][stellar]', () => {
  const node = String(process.env.STELLAR_NODE_URL);
  const account = 'GCZAOJPVYN5KTBUI6LJ5P7NCP4A43ZGQMBV2O6FB4MY7PD7J5XEU3P2X';
  const emptyAccount = 'GAYOLLLUIZE4DZMBB2ZBKGBUBZLIOYU6XFLW37GBP2VZD3ABNXCW4BVA';

  test('getStellarBalance() - Success', async () => {
    const balance = await core.balance.stellar(node, account);

    expect(balance.length).toBeGreaterThanOrEqual(0);
    expect(balance[0].decimals).toBe('10000000');
  });

  test('getStellarBalance() - Invalid account', async () => {
    await expect(core.balance.stellar(node, emptyAccount))
      .rejects
      .toThrow(`Unexpected error encountered when querying balance for account 'GAYOLLLUIZE4DZMBB2ZBKGBUBZLIOYU6XFLW37GBP2VZD3ABNXCW4BVA': Request failed with status code 404`);;
  });

  test('getStellarBalance() - Incorrect node url', async () => {
    await expect(core.balance.stellar('https://test.test', account))
      .rejects
      .toThrow(`Unexpected error encountered when querying balance for account 'GCZAOJPVYN5KTBUI6LJ5P7NCP4A43ZGQMBV2O6FB4MY7PD7J5XEU3P2X': getaddrinfo ENOTFOUND test.test`);;
  });
});
