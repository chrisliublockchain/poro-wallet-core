import core from '../../src';
import wallets from '../wallet.json';

describe('[balance][ripple]', () => {
  const account = wallets.ripple.address;
  const node = String(process.env.RIPPLE_NODE_URL);

  test('getRippleBalance() - Success', async () => {
    const balance = await core.balance.ripple(node, account);

    expect(balance.length).toBeGreaterThanOrEqual(0);
    expect(balance[0].ticker).toBe('XRP');
  });

  test('getRippleBalance() - Incorrect node url', async () => {
    await expect(core.balance.ripple('https://test.test', account))
      .rejects
      .toThrow(`Unexpected error encountered when querying account data for account '${account}': getaddrinfo ENOTFOUND test.test`);;
  });
});
