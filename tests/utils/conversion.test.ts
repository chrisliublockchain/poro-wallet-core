import core from '../../src';

describe('[utils][conversion]', () => {
  test('toHexString() - "123", 123, "0x0"', () => {
    const firstItem = core.utils.conversion.toHexString('123');
    const secondItem = core.utils.conversion.toHexString(123);
    const thirdItem = core.utils.conversion.toHexString('0x0');

    expect(firstItem).toBe('0x7b');
    expect(secondItem).toBe('0x7b');
    expect(thirdItem).toBe('0x0');
  });

  test('toHexString() - "abcdefg123"', () => {
    const hexString = () => core.utils.conversion.toHexString('abcdefg123');

    expect(hexString).toThrowError(`'abcdefg123' is not a valid HexString`);
  });

  test('toEvmAddress() - 0x81b7e08f65bdf5648606c89998a9cc8164397647', () => {
    const address = '0x81b7e08f65bdf5648606c89998a9cc8164397647';
    const evmAddress = core.utils.conversion.toEvmAddress(address);

    expect(evmAddress).toBe(address);
  });

  test('toEvmAddress() - 0x81b7e08f65bdf5648', () => {
    const address = '0x81b7e08f65bdf5648';
    const evmAddress = () => core.utils.conversion.toEvmAddress(address);

    expect(evmAddress).toThrowError(`'${address}' is not a valid EvmAddress`);
  });

  test('toRippleAddress() - r48Qm9cebfXE8wDwhb5t9WYDVH22AX1omv', () => {
    const address = 'r48Qm9cebfXE8wDwhb5t9WYDVH22AX1omv';
    const rippleAddress = core.utils.conversion.toRippleAddress(address);

    expect(rippleAddress).toBe(address);
  });

  test('toRippleAddress() - r48Qm9cebfXE8wDwhb22AX1omv', () => {
    const address = 'r48Qm9cebfXE8wDwhb22AX1omv';
    const rippleAddress = () => core.utils.conversion.toRippleAddress(address);

    expect(rippleAddress).toThrowError(`'${address}' is not a valid RippleAddress`);    
  });

  test('toStellarAddress() - GCXXM2W6CNMJMFMKAHLQOM7D73WTBF4ELPIXDKQBHGLO3V6WFDDIYWYV', () => {
    const address = 'GCXXM2W6CNMJMFMKAHLQOM7D73WTBF4ELPIXDKQBHGLO3V6WFDDIYWYV';
    const stellarAddress = core.utils.conversion.toStellarAddress(address);

    expect(stellarAddress).toBe(address);
  });

  test('toStellarAddress() - GDXSNY6M5KNXP3BJS4IHBJAEYCSSGDSVNIPNJCF6TBN3HMTODZG7YPSR', () => {
    const address = 'GDXSNY6M5KNXP3BJS4IHBJAEYCSSGDSVNIPNJCF6TBN3HMTODZG7YPSR';
    const stellarAddress = () => core.utils.conversion.toStellarAddress(address);

    expect(stellarAddress).toThrowError(`'${address}' is not a valid StellarAddress`);    
  });
});
