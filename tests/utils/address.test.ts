import { ethers } from 'ethers';

import core from '../../src';

describe('[utils][address]', () => {
  test('toChecksum() - 0x81b7e08f65bdf5648606c89998a9cc8164397647', () => {
    const address = '0x81b7e08f65bdf5648606c89998a9cc8164397647';
    const checksumAddress = core.utils.address.toChecksum(address);
    const ethersChecksumAddress = ethers.utils.getAddress(address);

    expect(checksumAddress).toBe(ethersChecksumAddress);
  });

  test('toChecksum() - 0x81b7e08f65bdf5648', () => {
    const address = '0x81b7e08f65bdf5648';
    const checksumAddress = () => core.utils.address.toChecksum(address);

    expect(checksumAddress).toThrowError(`'${address}' is not a valid EvmAddress`);
  });
});
