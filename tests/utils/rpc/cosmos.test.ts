import core from '../../../src';
import wallets from '../../wallet.json';

describe('[utils][rpc][cosmos]', () => {
  const account = wallets.cosmos.address;
  const node = String(process.env.COSMOS_NODE_URL);

  test('getCosmosAccountData() - Success', async () => {
    const data = await core.utils.rpc.cosmos.getCosmosAccountData(node, account);

    expect(typeof data).not.toBe('undefined');
    expect(typeof data.accountNumber).not.toBe('undefined');
    expect(typeof data.address).not.toBe('undefined');
    expect(typeof data.pubkey).not.toBe('undefined');
    expect(typeof data.sequence).not.toBe('undefined');
  });

  test('getCosmosAccountData() - Failed (new wallet generated without fund)', async () => {
    const wallet = await core.factory.wallet.cosmos.create();

    await expect(core.utils.rpc.cosmos.getCosmosAccountData(node, wallet.address))
      .rejects
      .toThrow(`Unexpected error encountered when querying account data for account '${wallet.address}'`);
  });

  test('getCosmosAccountData() - Failed (use malformed wallet address)', async () => {
    const wallet = { address: 'cosmos12345' };

    await expect(core.utils.rpc.cosmos.getCosmosAccountData(node, wallet.address))
      .rejects
      .toThrow(`'cosmos12345' is not a valid CosmosAddress`);
  });
});
