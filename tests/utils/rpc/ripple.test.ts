import core from '../../../src';
import wallets from '../../wallet.json';

describe('[utils][rpc][ripple]', () => {
  const account = wallets.ripple.address;
  const node = String(process.env.RIPPLE_NODE_URL);

  test('getRippleAccountData() - Success', async () => {
    const data = await core.utils.rpc.ripple.getRippleAccountData(node, account);

    expect(typeof data).not.toBe('undefined');
    expect(typeof data.Flags).not.toBe('undefined');
    expect(typeof data.Balance).not.toBe('undefined');
    expect(typeof data.Sequence).not.toBe('undefined');
  });

  test('getRippleAccountData() - Failed (new wallet generated without fund)', async () => {
    const wallet = await core.factory.wallet.ripple.create();

    await expect(core.utils.rpc.ripple.getRippleAccountData(node, wallet.address))
      .rejects
      .toThrow('Account not found.');
  });

  test('getRippleAccountData() - Failed (use malformed wallet address)', async () => {
    const wallet = { address: '0x123' };

    await expect(core.utils.rpc.ripple.getRippleAccountData(node, wallet.address))
      .rejects
      .toThrow(`'0x123' is not a valid RippleAddress`);
  });

  test(`getRippleAccountLines() - Success`, async () => {
    const accountLines = await core.utils.rpc.ripple.getRippleAccountLines(node, account);

    expect(typeof accountLines).not.toBe('undefined');
  });

  test('getRippleAccountLines() - Failed (new wallet generated without fund)', async () => {
    const wallet = await core.factory.wallet.ripple.create();

    await expect(core.utils.rpc.ripple.getRippleAccountLines(node, wallet.address))
      .rejects
      .toThrow('Account not found.');
  });

  test('getRippleAccountLines() - Failed (use malformed wallet address)', async () => {
    const wallet = { address: '0x123' };

    await expect(core.utils.rpc.ripple.getRippleAccountLines(node, wallet.address))
      .rejects
      .toThrow(`'0x123' is not a valid RippleAddress`);
  });
});
