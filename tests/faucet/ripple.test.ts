import core from '../../src';
import wallets from '../wallet.json';

describe('[faucet][ripple]', () => {
  const account = wallets.ripple.address;

  // Have rate limit, won't be running it in CI pipeline constantly
  xtest('getFundFromRippleFaucet() - Success', async () => {
    const result = await core.faucet.ripple(account);

    expect(result.success).toBe(true);
    expect(typeof result.timestamp).not.toBe('undefined');
    expect(typeof result.message).toBe('undefined');
  });
});
