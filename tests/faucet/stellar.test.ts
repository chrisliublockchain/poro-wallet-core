import core from '../../src';
import wallets from '../wallet.json';

describe('[faucet][stellar]', () => {
  const account = wallets.stellar.address;

  // Have rate limit, won't be running it in CI pipeline constantly
  xtest('getFundFromStellarFaucet() - Success', async () => {
    const result = await core.faucet.stellar(account);

    expect(result.success).toBe(true);
    expect(typeof result.timestamp).not.toBe('undefined');
    expect(typeof result.message).toBe('undefined');
  });
});
