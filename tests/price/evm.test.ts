import core from '../../src';

describe('[price][evm]', () => {
  const node = String(process.env.ETHEREUM_GOERLI_NODE_URL);

  test('fee()', async () => {
    const fee = await core.price.evm.fee(node);

    expect(typeof fee.maxFeePerGas).toBe('string');
    expect(typeof fee.maxPriorityFeePerGas).toBe('string');
  });
});
