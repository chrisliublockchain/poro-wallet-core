import core from '../../src';

describe('[price][ripple]', () => {
  const node = String(process.env.RIPPLE_NODE_URL);

  test('fee()', async () => {
    const fee = await core.price.ripple.fee(node);

    expect(typeof fee).toBe('string');
  });

  test('token()', async () => {
    // Testnet USD
    const currency = 'USD';
    const contractAddress = 'rBZJzEisyXt2gvRWXLxHftFRkd1vJEpBQP';
    const tokenPrice = await core.price.ripple.token(node, currency, contractAddress);

    expect(typeof tokenPrice).toBe('string');
  });
});
