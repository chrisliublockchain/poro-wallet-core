module.exports = {
  preset: 'ts-jest',
  testTimeout: 1000000,
  coverageThreshold: {
    "global": {
      "branches": 50,
      "functions": 50,
      "lines": 50,
    }
  },
  collectCoverage: true,
  testEnvironment: 'node',
  coverageDirectory: 'coverage',
  setupFiles: ["dotenv/config"],
  testMatch: ["**/?(*.test.ts)"],
  moduleFileExtensions: ["js", "ts", "json", "node"],
};
